//
//  ViewController.swift
//  TrackISS
//
//  Created by Bernardo Tavera on 2/18/18.
//  Copyright © 2018 Bernardo Tavera. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController, UITableViewDelegate, CLLocationManagerDelegate {
    
    // MARK: - UI Components
    
    @IBOutlet var mpView: MKMapView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var tableView: UITableView!
    
    @IBAction func btn_Action_Map(_ sender: UIButton) {
        mpView.isHidden = false
        detectGPS()
        btn_Map.isHidden = true
        btn_Info.isHidden = false
        tableView.isHidden = true
        label_info.text = "Click on ISS Info button to display ISS tracking information."
    }
    @IBOutlet var btn_Map: UIButton!
    
    @IBAction func btn_Action_Info(_ sender: UIButton) {
        tableView.isHidden = false
        gps_info = []
        gps_values = []
        gps_duration = []
        gps_risetime = []
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        getTracker(latitude: gps_latitude, longitude: gps_longitude)
        btn_Map.isHidden = false
        btn_Info.isHidden = true
        mpView.isHidden = true
        label_info.text = "Click on Map Location button to locate your device."
    }
    @IBOutlet var btn_Info: UIButton!
    
    @IBOutlet var label_info: UILabel!
    
    // MARK: - Data model structure
    
    enum BackendError: Error {
        case urlError(reason: String)
        case objectSerialization(reason: String)
    }
    
    struct Detail : Codable {
        let altitude: Int
        let datetime: Int
        let latitude: Double
        let longitude: Double
        let passes: Int
    }
    
    struct Info : Codable  {
        let duration:Int
        let risetime:Int
    }
    
    struct Tracker : Codable  {
        let request : Detail
        let response :[Info]
        
        //URLs
        static func endpointForGPS(latitude lat:Double , longitude long: Double) -> String {
            return "http://api.open-notify.org/iss-pass.json?lat=\(lat)"+"&lon=\(long)"
        }
        
        //Load data
        
        /**
         Loads ISS passes information based on device gps location
         
         - parameter latitude: device latitude location
         - parameter longitude: device longitude location
         */
        
        static func trackByGPS(latitude lat:Double , longitude long: Double, completionHandler: @escaping (Tracker?, Error?) -> Void) {
            
            if !Reachability.isConnectedToNetwork() {
                print("Error: no internet signal")
                let error = BackendError.urlError(reason: "no internet signal")
                completionHandler(nil, error)
                return
            }
            
            let endpoint = Tracker.endpointForGPS(latitude:lat,longitude:long)
            guard let url = URL(string: endpoint) else {
                print("Error: cannot create URL")
                let error = BackendError.urlError(reason: "Could not create URL")
                completionHandler(nil, error)
                return
            }
            let urlRequest = URLRequest(url: url)
            
            let session = URLSession.shared
            
            let task = session.dataTask(with: urlRequest, completionHandler: {
                (data, response, error) in
                guard let responseData = data else {
                    print("Error: did not receive data")
                    completionHandler(nil, error)
                    return
                }
                guard error == nil else {
                    completionHandler(nil, error!)
                    return
                }
                
                let decoder = JSONDecoder()
                do {
                    let tracker = try decoder.decode(Tracker.self, from: responseData)
                    completionHandler(tracker, nil)
                } catch {
                    print("error trying to convert data to JSON")
                    print(error)
                    completionHandler(nil, error)
                }
            })
            task.resume()
        }
        
    }
    
    // MARK: - Variable listing
    
    var gps_info = [Dictionary<String,String>]()
    var gps_test = [Info]()
    var gps_duration = [Int]()
    var gps_risetime = [String]()
    var gps_detail = [Detail]()
    var gps_values = [String]()
    var locationManager: CLLocationManager = CLLocationManager()
    var gps_longitude: Double = 0.0
    var gps_latitude: Double = 0.0
    
    
    // MARK: - viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.isHidden = true
        btn_Info.isHidden = true
        label_info.text = "Click on Map Location button to locate your device."
        
    }
    
    // MARK: - viewDidAppear
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !Reachability.isConnectedToNetwork() {
            mpView.isHidden = true
            btn_Map.isHidden = true
            btn_Info.isHidden = true
            label_info.isHidden = true
            activityIndicator.isHidden = true
            showErrorAlert(with: "No internet signal. Please get access to internet and restart the application.")
        }
    }
    
    //Track ISS based on Device GPS location
    
    /**
     Loads ISS passes information on the TableView to display the content
     
     - parameter latitude: device latitude location
     - parameter longitude: device longitude location
     */
    
    func getTracker(latitude lat:Double , longitude long: Double) {
        Tracker.trackByGPS(latitude: lat,longitude: long, completionHandler: { (tracker, error) in
            if let error = error {
                // got an error in getting the data, need to handle it
                self.showErrorAlert(with: "There is an error with the connectivity to the ISS Station. Please try later.")
                print(error)
                return
            }
            guard let tracker = tracker else {
                //print("error getting tracker: result is nil")
                self.showErrorAlert(with: "The ISS station is not sending data at this time. Please try later")
                return
            }
            // success
            DispatchQueue.main.async {
                
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
                let altitude = tracker.request.altitude
                let datetime = tracker.request.datetime
                let latitude = tracker.request.latitude
                let longitude = tracker.request.longitude
                let passes = tracker.request.passes
                var item: [String: String] = [:]
                self.gps_test = tracker.response
                self.gps_detail.append(tracker.request)
                
                for case let result in tracker.response {
                    self.gps_duration.append(result.duration)
                    self.gps_risetime.append(self.dateConversion(result.risetime))
                    }
                
                // TODO: - Pending verify if values are correct
                
                item = ["altitude" : String(altitude)]
                self.gps_info.append(item)
                self.gps_values.append(String(altitude))
                item = ["datetime" : self.dateConversion(datetime)]
                self.gps_info.append(item)
                self.gps_values.append(self.dateConversion(datetime))
                item = ["latitude" : String(latitude)]
                self.gps_info.append(item)
                self.gps_values.append(String(latitude))
                item = ["longitude" : String(longitude)]
                self.gps_info.append(item)
                self.gps_values.append(String(longitude))
                item = ["passes" : String(passes)]
                self.gps_info.append(item)
                self.gps_values.append(String(passes))
                
                print("gps_info :",self.gps_info)
                print("gps_duration :",self.gps_duration)
                print("gps_risetime :",self.gps_risetime)
                print("count :",self.gps_info.count)
                print("count duration:",self.gps_duration.count)
                self.tableView.reloadData()
                
            }
            
        })
    }

    // MARK: - Helpers
    
    func dateConversion (_ dateData:Int) -> String{
        
        let date = Date(timeIntervalSince1970: TimeInterval(dateData))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm" //Specify date
        let strDate = dateFormatter.string(from: date)
        
        return strDate
        
    }
    
    private func showErrorAlert(with message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - CLLocationManagerDelegate
    
    func detectGPS () {
        
        //For use when the app is open & in the background
        locationManager.requestAlwaysAuthorization()
        
        //For use when the app is open
        //locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    //Check authorization

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            locationManager.startUpdatingLocation()
        }
    }
 
    //Display location on map
    
    /**
     Return the device gps coordinates (longitude and latitude ) based on CoreLocation framework
     */
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            
            print(location.coordinate)
            
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.20, longitudeDelta: 0.20))
            
            //Set region of current location in map view with zooming
            self.mpView.setRegion(region, animated: true)
            
            //Show current location (blue dot) on map
            self.mpView.showsUserLocation = true
            
            //Assign longitude and latitude of device to track ISS
            gps_longitude = location.coordinate.longitude
            gps_latitude = location.coordinate.latitude
            
        }
    }
    
    // MARK: - didReceiveMemoryWarning
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    
    }
    
}

// MARK: - TableViewDelegate

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch (section) {
        case 0:
            return gps_info.count
        case 1:
            return gps_duration.count
        default:
            return 0
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch (section) {
        case 0:
            return "GPS Device Information"
        case 1:
            return "ISS-PASS-Times"
        default:
            return "No Title"
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "gpsCell", for: indexPath)
       
        switch (indexPath.section) {
        case 0:
            let gp = gps_info[indexPath.row]
            let strKey = [String] (gp.keys)
            cell.textLabel?.text = strKey[0]
            cell.detailTextLabel?.text = gps_values[indexPath.row]
        case 1:
            let duration = gps_duration[indexPath.row]
            let risetime = gps_risetime[indexPath.row]
            cell.textLabel?.text = "Date risetime: \(risetime)"
            cell.detailTextLabel?.text = "\(String(duration)) Seconds"
        default:
            print("No option")
        }
        return cell
        
    }
}

