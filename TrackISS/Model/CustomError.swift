//
//  CustomError.swift
//  TrackISS
//
//  Created by Bernardo Tavera on 2/19/18.
//  Copyright © 2018 Bernardo Tavera. All rights reserved.
//

import Foundation

struct CustomError: Error, Codable {
    var message: String
}
